\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage{amssymb, amsmath, graphicx}
\usepackage[ruled,noline]{algorithm2e}
\usepackage{hyperref}
\usepackage{bbm}

\setlength{\oddsidemargin}{.25in}
\setlength{\evensidemargin}{.25in}
\setlength{\textwidth}{6in}
\setlength{\topmargin}{-0.4in}
\setlength{\textheight}{8.5in}

\newcommand{\heading}[6]{
  \renewcommand{\thepage}{\arabic{page}}
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { \textbf{#2} \hfill #3 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #6  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { \textit{Collaborators: #4 \hfill #5} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\newenvironment{proof}{\noindent{\bf Proof:} \hspace*{1mm}}{
	\hspace*{\fill} $\Box$ }
\newenvironment{proof_of}[1]{\noindent {\bf Proof of #1:}
	\hspace*{1mm}}{\hspace*{\fill} $\Box$ }
\newenvironment{proof_claim}{\begin{quotation} \noindent}{
	\hspace*{\fill} $\diamond$ \end{quotation}}

\newcommand{\lecturenotes}[5]{\heading{#1}{6.875J/18.425J: Cryptography and Cryptanalysis}{#2}{#5}{#3}{#4}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE MODIFY THESE FIELDS AS APPROPRIATE
\newcommand{\psetnum}{1}          % lecture number
\newcommand{\lecturetitle}{Problem set 1} % topic of lecture
\newcommand{\lecturedate}{\today}  % date of lecture
\newcommand{\studentname}{Michael Gharbi}    % full name of student (i.e., you)
\newcommand{\collaborators}{Andrea Tacchetti, Frank Yi-Fei Wang}    % full name of student (i.e., you)

\newcommand*\xor{\mathbin{\oplus}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\lecturenotes{\psetnum}{\lecturedate}{\studentname}{\lecturetitle}{\collaborators}


\section{Shannon's Theorem}

\textbf{(a)} We have for $SK$ sample uniformly and independtly in $\mathcal{K}$ and $C$ sampled
 in the ciphertexts:

\begin{eqnarray*}
    \mathbb{P}_{SK,C,M}[D(SK,C)=M] &=&
    \sum_{sk,c,m}\mathbbm{1}_{D(sk,c)=m}\mathbb{P}[SK=sk;C=c;M=m]\\
    &=&\sum_{sk,c,m}\mathbbm{1}_{D(sk,c)=m}\mathbb{P}[SK=sk;C=c]\mathbb{P}[M=m]\\
    &\leq&\sum_{sk,c}\mathbb{P}[SK=sk;C=c]\max_{m\in\mathcal{M}}(\mathbb{P}[M=m])\\
    &=&\max_{m\in\mathcal{M}}(\mathbb{P}[M=m])\\
\end{eqnarray*}

The second equality comes from perfect secrecy, which makes $C$ and $M$
independent. On the other side, we also have:

\begin{eqnarray*}
    \mathbb{P}_{SK,C,M}[D(SK,C)=M] &=& \mathbb{P}_{SK,SK',M}[D(SK,E(SK',M))=M] \\
    &=& \sum_{k,k',m}\mathbbm{1}_{D(k,E(k',m))=m}\mathbb{P}[SK=k;SK'=k';M=m]\\
    &\geq& \sum_{k,k',m}\mathbbm{1}_{D(k,E(k',m))=m}\mathbbm{1}_{k=k'}\mathbb{P}[SK=k]\mathbb{P}[SK'=k';M=m]\\
    &=& \sum_{k,m}\mathbbm{1}_{D(k,E(k,m))=m}\mathbb{P}[SK'=k';M=m]\frac{1}{|\mathcal{K}|}\\
    &=& \mathbb{P}_{SK,M}[D(SK,E(SK,M))=M]\frac{1}{|\mathcal{K}|}\\
    &\geq& \frac{1-\epsilon}{|\mathcal{K}|}\\
\end{eqnarray*}

The first equality follows the fact that $C$ and $E(SK',M)$ have the same
distribution. Then $SK$ is chosen independently.

Thus we get:
\begin{equation*}
    |\mathcal{K}|\geq(1-\epsilon)(\max_{m\in\mathcal{M}}(\mathbb{P}[M=m]))^{-1}
\end{equation*}

And if $\mathbb{P}_M$ is uniform:
\begin{equation*}
    |\mathcal{K}|\geq(1-\epsilon)|\mathcal{M}|
\end{equation*}

\textbf{(b)} Let us prove the following bound :
$|\mathcal{K}|\geq|\mathcal{M}|$. Consider the set $\mathcal{M}_c$ defined as:

\begin{equation}
    \mathcal{M}_c = \{m\in\mathcal{M}\ s.t.\ \exists k,\ m=D(k,c)\}
\end{equation}

Suppose $|\mathcal{K}|<|\mathcal{M}|$, then as $|\mathcal{M}_c|$ is bounded by
the number of keys, we have $|\mathcal{M}_c|\leq|\mathcal{K}|<|\mathcal{M}|$.
This implies this existence of $m\in\mathcal{M}\backslash\mathcal{M}_c$. As the
probability distribution in the relaxed definition of security is arbitrary, we
can pick a distribution such that $\mathbb{P}[M=m] = \epsilon +\delta$ for some
$\delta > 0$. From the definition of $\mathcal{M}_c$ we get $\mathbb{P}[M=m|C=c]
= 0 $. Injecting these two values in the definition of almost perfect secrecy,
we obtain the following contradiction:

\begin{equation}
    |\mathbb{P}[M=m] - \mathbb{P}[M=m|C=c]| = \epsilon + \delta > \epsilon
\end{equation}

This conclude the proof and shows that $|\mathcal{K}|\geq|\mathcal{M}|$ is a lower bound.


\section{Definitions of Security}

We will use the following definition of statistical security from David Wagner's
spring 2006 class at
Berkeley\footnote{\url{http://www.cs.berkeley.edu/~daw/teaching/cs276-s06/l2.ps}
definition 2.2}:

\begin{definition}
    Statistical security: $\forall \mathcal{S}\subseteq\mathcal{C}$ subset of
    the ciphertexts, $\exists \epsilon$ negligible s.t.,$\forall
    m_0,m_1\in\mathcal{M}$:
    \begin{equation*}
        |\mathbb{P}_{SK}[E(SK,m_0)\in\mathcal{S}]-\mathbb{P}_{SK}[E(SK,m_1)\in\mathcal{S}]|\leq \epsilon
    \end{equation*}
    \label{def:stat_sec}
\end{definition}

Definition \ref{def:stat_sec}, implies the following definition (they are in
fact equivalent):

\begin{definition}
    Statistical security - equivalent definition: $\forall \mathcal{A}$
    (time-unbounded) algorithm, $\exists \epsilon$ negligible s.t.,$\forall
    m_0,m_1\in\mathcal{M}$:
    \begin{equation*}
        |\mathbb{P}_{SK}[\mathcal{A}(E(SK,m_0))=1]-\mathbb{P}_{SK}[\mathcal{A}(E(SK,m_1))=1]|\leq \epsilon
    \end{equation*}
    \label{def:stat_sec_2}
\end{definition}

For $\mathcal{A}$, consider $\mathcal{S}=\mathcal{A}^{-1}(1)$ and apply
definition \ref{def:stat_sec} and the assertion follows.\\

\textbf{(b)} by restricting $\mathcal{A}$ to be a PPT algorithm and using
definition \ref{def:stat_sec_2}, we have immediately that \textit{statistical security} implies
\textit{computational indistinguishability}.

\textbf{(a)} by using $m_1$=$m_0+1\ mod\ 2^k$ in the definition of
\textit{computational indistinguishability}, we have \textit{statistical
security}$\Rightarrow$\textit{computational
indistinguishability}$\Rightarrow$\textit{neighbor indistinguishability}. This
concludes the proof.

\section{OWFs and Secure Encryption}

Our proof will be based on the fact that \textit{semantic security} is
equivalent to \textit{message indistinguishability}. A proof of this fact can be
found in the lecture notes of Luca Trevisan,
Stanford\footnote{\url{http://theory.stanford.edu/~trevisan/books/crypto.pdf}
section 2.1}.

\begin{definition}
    Message indistinguishability $\forall \mathcal{A}$
    PPT algorithm, $\exists \epsilon$ negligible s.t.:
    \begin{eqnarray*}
    |\mathbb{P}_{SK}[\mathcal{A}(m_0,m_1,c))=1|(m_0,m_1)\leftarrow
\{0;1\}^k,c\leftarrow E(SK,m_1)]-\\
\mathbb{P}_{SK}[\mathcal{A}(m_0,m_1,c))=1|(m_0,m_1)\leftarrow
\{0;1\}^k,c\leftarrow E(SK,m_0)]|\leq \epsilon
    \end{eqnarray*}
    \label{def:mess_indi}
\end{definition}

We will first show that if $(G,E,D)$ is message indistinguishable, it is also
one way in the sense of definition \ref{def:ows}:

\begin{definition}
 One-way secure encryption $\forall \mathcal{A}$
    PPT algorithm, $\exists \epsilon$ negligible s.t.:
    \begin{equation*}
    \mathbb{P}_{SK}[\mathcal{A}(c))=m|m\leftarrow
\{0;1\}^k,c\leftarrow E(SK,m)]\leq \epsilon
    \end{equation*}
    \label{def:ows}
\end{definition}

Suppose that the encryption scheme $(G,E,D)$ is not one-way secure. Let us prove
that it can't be message indistinguishable. If the scheme is not one-way secure,
there exists a PPT algorithm $\mathcal{A}$ that breaks definition \ref{def:ows}
with non negligible probability, let us say greater than $p$.

We build algorithm $\mathcal{A}'$ as follows :
\begin{itemize}
    \item On input $(m_0,m_1,c)$ we run $\mathcal{A}(c)$.
    \item if $\mathcal{A}(c)=m_1$ we output $1$ and $0$ otherwise.
\end{itemize}

On the one hand, we have for $m_1$:
\begin{eqnarray}
\mathbb{P}_{SK}[\mathcal{A}'(m_0,m_1,c))=1|(m_0,m_1)\leftarrow
\{0;1\}^k,c\leftarrow E(SK,m_1)] =\\
\mathbb{P}_{SK}[\mathcal{A}(c))=m_1|m_1\leftarrow
\{0;1\}^k,c\leftarrow E(SK,m_1)]\geq p
\end{eqnarray}

On the other hand, for $m_0$:
\begin{eqnarray}
\mathbb{P}_{SK}[\mathcal{A}'(m_0,m_1,c))=1|(m_0,m_1)\leftarrow
\{0;1\}^k,c\leftarrow E(SK,m_0)] =\\
\mathbb{P}_{SK}[\mathcal{A}(c))=m_1|m_1\leftarrow
\{0;1\}^k,c\leftarrow E(SK,m_0)]\leq 2^{-k}
\end{eqnarray}

The last inequality comes from the fact that $m_1$ is chosen at random
(uniformly) in $\mathcal{M}$ and $\mathcal{A}(c)$ is independent of $m_1$.

The difference between these two terms is then greater than $p-2^{-k}$ which is
non negligible and therefore breaks definition \ref{def:mess_indi}.

We will now prove that the existence of a scheme that satisfies definition
\ref{def:ows} implies the existence of a collection of one-way functions. This
will wrap up the proof.

We consider the set of indexes $\mathcal{I}=\{1^k\}_{k\in\mathbb{N}}$. We
rewrite the PPT algorithms $G,E$ to make explicit their random coins as follows:

\begin{eqnarray*}
    K: 1^k\times\{0;1\}^{q(k)}\rightarrow\{0;1\}^k & K(1^k,r_1) = SK\\
    E: \{0;1\}^{k}\times\{0;1\}^{2*k}\times\{0;1\}^{s(k)} & E(SK,m,r_2) = c
\end{eqnarray*}

With some polynomials $q,s$. We then define our collection of functions
$\{f_k\}_{k\in\mathcal{I}}$ as:
\begin{eqnarray*}
    f_k: \{0;1\}^{2k}\times \{0;1\}^{q(k)} \times\{0;1\}^{s(k)} \rightarrow
    \{0;1\}^{t(k)} & f_k(m,r_1,r_2) = E(K(1^k,r_1),m,r_2)
\end{eqnarray*}

Clearly, we have $\mathcal{I}\cap\{0;1\}^k$ is easy to sample, the domain of
$f_k$ knowing $k$ can be sample in polynomial time and $f_k(x)$ is easy to
evaluate once $x,k$ are sampled. It remains to prove that $f_k$ is one way.
Suppose it is not. Then there exists a PPT algorithm $\mathcal{B}$ such that
$\mathcal{B}(f_k(m,r_1,r_2))$ outputs $(m',r'_1,r'_2)$ such that
$f_k(m,r_1,r_2)=f_k(m',r'_1,r'_2)$ with non negligible probability.

We construct $\mathcal{B'}$ that violates definition \ref{def:ows} as follows:
\begin{itemize}
    \item on input $c$, run $\mathcal{B}(c)$
    \item returns $m'$
\end{itemize}

Like $\mathcal{B}$, $\mathcal{B}'$ succeeds with non negligible probability and
therefore violates definition \ref{def:ows}: contradiction, $\{f_k\}_k$ is a
collection of one-way functions. In conclusion, \textit{message
indistinguishability} implies \textit{one-way security} which in turns implies
the existence of a collection of one-way functions.




\section{One-Way Functions}

We consider $f$, a length preserving one-way function. By definition, it is
possible to compute $f$ efficiently. The bitwise complement and xor operation
are, of course, computable in polynomial time. It follows that the functions $g$ proposed
are all easy to compute. To prove that there are one-way (resp. not one-way), we
will only need to demonstrate they are hard to invert (resp. easy).\\


\textbf{(a)} g \textbf{is not} necessarily one-way. Define $f$ as $f(x_1 | x_2) =
h(x_2)|0^n$, for some length-preserving one-way function $h$, where $|x_1| =
|x_2| = n$. Composing $f$ with itself, we get $f(f(x_1|x_2)) = f(h(x_2)|0^n) =
h(0^n)|0^n$. Therefore, the range of $f\circ f$ is $\{h(0^n)|0^n\}$ and the
function is clearly easy to invert, as any $x=x_1|x_2$ will do. 

If we prove that the $f$ defined above is one way, we will contradict the
proposition, showing that $f$ is one-way does not necessarily implies $g=f\circ f$
is one-way.

Suppose $f$ defined above is not one-way. Then there exists a PPT algorithm $\mathcal{A}$ such
that $\mathcal{A}(f(x))=x'\in f^{-1}(f(x))$ with non negligible probability.
We can then build an algorithm $\mathcal{B}$ that inverts $h$ with non
negligible probability: given $h(x)$ as input, we run $\mathcal{A}$ on
$h(x)|0^n$, which returns $x_1'|x_2'$ s.t. $f(x_1'|x_2') = h(x_2')|0^n$ with
sufficiently high probability. Then $\mathcal{B}$ returns $x_2'$ and therefore
inverts $h$ with non negligible probability. This concludes the proof.\\

\textbf{(b)} Let us show that $g(x)=f(\bar{x})$ \textbf{is} a one-way function. Suppose
this is not the case and $\mathcal{A}$ is a PPT algorithm that inverts $g$ with
non negligible probability. We define, algorithm $\mathcal{B}$ as such: given
$f(x)$ as input, we run $\mathcal{A}$ and retrieve $x'$ such that $g(x') =
g(\bar{x}) = f(x)$. $\mathcal{B}$ returns $\bar{x'}$ and therefore successfully
inverts $f$ with non negligible probability and in polynomial time. This means
that $f$ is not one-way : contradiction. We conclude that $g$ is one-way.\\

\textbf{(c)} g \textbf{is not} necessarily one-way. Define $f$ as $f(x,y) =
(h(x)\xor y,y)$, for some length-preserving one-way function $h$ ($f$ is also
length preserving). For this $f$, we have $g(x,y) = f(x,y)\xor(x,y) = (h(x)\xor
y\xor x, y\xor y) = (h(x)\xor y\xor x, 0^n)$ (using component-wise xoring). 

This function is easy to invert: if we pick any $(a,0^n)$ in its range, the pair
$(0^n,a\xor h(0^n))$ is an acceptable pre-image as $f(0,a\xor h(0^n))= (h(0^n)\xor a\xor
h(0^n)\xor 0^n, 0^n) = (a,0)$. An this can be done in polynomial time as $h$ is
supposed to be a one-way function.

If we prove that the $f$ defined above is one way, we will contradict the
proposition, showing that $f$ is one-way does not necessarily implies $g(x) =
f(x)\xor x$ is one-way.

Suppose $f$ defined above is not one-way. Then there exists a PPT algorithm $\mathcal{A}$ such
that $\mathcal{A}(f(x,y))=(x',y')\in f^{-1}(f(x,y))$ with non negligible probability.
We can then build an algorithm $\mathcal{B}$ that inverts $h$ with non
negligible probability: given $h(x)$ as input, we run $\mathcal{A}$ on
$(h(x)\xor y,y)$ for a random $y$, which returns $x',y'$ s.t. $(h(x')\xor y',y') = f(x',y') =
f(x,y) = (h(x) \xor y, y)$ with sufficiently high probability. In particular,
$y'=y$
and thus $h(x) = h(x')$ Then $\mathcal{B}$
returns $x'$ and therefore inverts $h$ with non negligible probability. This
concludes the proof.\\

\textbf{(d)} Let us show that $g(x,y) = (f(x\xor y))$ \textbf{is} a one-way function. Suppose
this is not the case and $\mathcal{A}$ is a PPT algorithm that inverts $g$ with
non negligible probability. We define, algorithm $\mathcal{B}$ as such: given
$f(x)$ as input, we run $\mathcal{A}$ on $f(x\xor 0^n)$ and retrieve $x',y'$
such that $f(x'\xor y') =g(x',y') = g(x,0^n) = f(x\xor 0^n) = f(x)$.
$\mathcal{B}$ returns $x'\xor y'$ and therefore successfully inverts $f$ with non
negligible probability and in polynomial time. This means that $f$ is not
one-way: contradiction. We conclude that $g$ is one-way.\\

\textbf{(e)} g \textbf{is not} necessarily one-way. Define $f$ as:

\begin{equation*}
    f(x_1|x_2) = \left\{ \begin{array}{l l}
            h(x_1)|x_2 & \text{if $x_1|x_2 = 1\ mod\ 2$}\\
            h(x_2)|x_1 & \text{otherwise}\\
\end{array} \right.
\end{equation*}

for some length-preserving one-way function $h$ ($f$ is also
length preserving). For this $f$, we have: 
\begin{equation*}
    g(x_1|x_2) = f(x_1|x_2)|f(\bar{x}_1|\bar{x}_2) = \left\{ \begin{array}{l l}
            h(x_1)|x_2|h(\bar{x}_2)|\bar{x}_1 & \text{if $x_1|x_2 = 1\ mod\ 2$}\\
            h(x_2)|x_1|h(\bar{x}_1)|\bar{x}_2 & \text{otherwise}\\

\end{array} \right.
\end{equation*}

This function is clearly easy to invert as it contains the input in clear in
both cases. If we prove that the $f$ defined above is one way, we will
contradict the proposition, showing that $f$ is one-way does not necessarily
implies $g(x) = f(x)|f(\bar{x})$ is one-way.

Suppose $f$ defined above is not one-way. Then there exists a PPT algorithm
$\mathcal{A}$ such that $\mathcal{A}(f(x))=x'\in f^{-1}(f(x))$ with non
negligible probability. We can then build an algorithm $\mathcal{B}$ that
inverts $h$ with non negligible probability: given $h(x)$ as input, we run
$\mathcal{A}$ on $h(x)|y$ for some $y$ (we can assume $y\ mod\ 2 = 1$), which
returns $x',y'$ such that $f(x'|y') = f(x|y)$, i.e.:

\begin{equation*}
    h(x)|y = \left\{ \begin{array}{l l}
            h(x')|y' & \text{if $y' = 1\ mod\ 2$}\\
            h(y')|x' & \text{otherwise}\\

\end{array} \right.
\end{equation*}

with sufficiently high probability. We compute $h(x')$ and $h(y')$, which can be
done in polynomial time as $h$ is one-way. One of these will be equal to $h(x)$
and we therefore return $x'$ or $y'$. $\mathcal{B}$ inverts $h$ with non
negligible probability. This concludes the proof.\\

\section{Number theory}

Let us prove that the Differential Diffie Hellman problem is not hard in
$\mathbb{Z}_p^*$ with $p$ prime and $g$ a generator. More specifically, define
$\{DDH_n\}_n$ and $\{R_n\}_n$ the two distribution ensemble, respectively of
triplets $(g^x,g^y,g^{xy})$ and $(g^x,g^y,g^r)$ with $x,y,r$ generated uniformly
at random in $\{1\ldots p-1\}$, indexed by $n=|p|$. Let us show that
computational indistinguishability does not hold, i.e. we are going to
contradict the following definition:

\begin{definition}
$\forall\mathcal{A}$ PPT algorithm , there exists a negligible function
$\epsilon$ such that:

\begin{equation}
    |\mathbb{P}_{x\leftarrow DDH_n}[A(x) = 1]-\mathbb{P}_{x\leftarrow R_n}[A(x) =
    1]| < \epsilon(n)
    \label{eq:comp_ind}
\end{equation}
\end{definition}

We build an algorithm $\mathcal{A}$ as follows:
\begin{itemize}
    \item On input $(g^x,g^y,g^z)$, we compute the Legendre symbol $\mathcal{L}$ for each
element of the triplet, which can be done efficiently with $\mathcal{L}(x) =
x^{\frac{p-1}{2}}$, and map it as $1\rightarrow 1$, $-1 \rightarrow 0$ for
convenience.
    \item $\mathcal{A}$ outputs $1$ if
        $\mathcal{L}(g^z)=\mathcal{L}(g^x)||\mathcal{L}(g^y)$, and $0$ otherwise.
\end{itemize}

In the case were $X = (g^x,g^y,g^z)\leftarrow DDH_n$, it is easy to see that
$\mathcal{A} = 1$ with probability $1$. But when, $X\leftarrow R_n$, this
probability is $\frac{1}{2}$ as $z$ is sampled uniformly at random,
independently form $x,y$. Therefore, the difference in equation
\ref{eq:comp_ind} is $\frac{1}{2}$ which is non negligible. This concludes the
proof. 

One could go further and use random self-reducibility on carefully chosen random
samples built from $X$ and show that this advantage can lead to the ability of deciding in
polynomial time and with overwhelming probability for each instance seen, which distribution generated it.


\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
