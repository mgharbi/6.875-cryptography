\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage{amssymb, amsmath, graphicx}
\usepackage[ruled,noline]{algorithm2e}
\usepackage{hyperref}
\usepackage{bbm}
\usepackage{indentfirst}

\setlength{\oddsidemargin}{.25in}
\setlength{\evensidemargin}{.25in}
\setlength{\textwidth}{6in}
\setlength{\topmargin}{-0.4in}
\setlength{\textheight}{8.5in}

\newcommand{\heading}[6]{
  \renewcommand{\thepage}{\arabic{page}}
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { \textbf{#2} \hfill #3 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #6  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { \textit{Collaborators: #4 \hfill #5} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\newenvironment{proof}{\noindent{\bf Proof:} \hspace*{1mm}}{
	\hspace*{\fill} $\Box$ }
\newenvironment{proof_of}[1]{\noindent {\bf Proof of #1:}
	\hspace*{1mm}}{\hspace*{\fill} $\Box$ }
\newenvironment{proof_claim}{\begin{quotation} \noindent}{
	\hspace*{\fill} $\diamond$ \end{quotation}}

\newcommand{\lecturenotes}[5]{\heading{#1}{6.875J/18.425J: Cryptography and Cryptanalysis}{#2}{#5}{#3}{#4}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE MODIFY THESE FIELDS AS APPROPRIATE
\newcommand{\psetnum}{4}          % lecture number
\newcommand{\lecturetitle}{Problem set 6} % topic of lecture
\newcommand{\lecturedate}{\today}  % date of lecture
\newcommand{\studentname}{Michael Gharbi}    % full name of student (i.e., you)
\newcommand{\collaborators}{Andrea Tacchetti, Frank Yi-Fei Wang, Hamza Fawzi}    % full name of student (i.e., you)

\newcommand*\xor{\mathbin{\oplus}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\lecturenotes{\psetnum}{\lecturedate}{\studentname}{\lecturetitle}{\collaborators}

\section{Secret Sharing}
    Our scheme will use Shamir's secret sharing scheme as a building block. In
    Shamir's $t$-out-of-$n$ scheme, if the secret $s$ is divided in $n$ shares, it can be
    reconstructed from $t$ pieces.

    In our case, we divide the secret $s$ in $n=2m$ pieces which we distribute
    (using Shamir's) to all $2m$ player in $X\cup Y$ with $t=\frac{9}{10}m$.
    Additionally we generate $u,x$ at random with the same length as $s$ and let
    $k=u\xor s$. We share $k$ using Shamir's scheme with the $2m$ player and
    $t=\frac{1}{5}m$. We set $y=x \xor u$. Each player from $X$ (reps. $Y$) receives
    $x$ (resp. $y$).

    Any corruptible set cannot gain information on $s$ and any non-corruptible set
    can recover s. If strictly less than $\frac{9}{10}m$ players, all from $X$ (or similarly, all from
    $Y$) collude, they have access to $x$ and $k$. They can't recover $u$, and
    therefore cannot get access to $s$ since the one-time-pad is unconditionally
    secure. This follows from the guarantees of Shamir's scheme. However if
    $\frac{9}{10}m$ of the players from the same set collude, they can retrieve $s$.
    (this follows from the first use of Shamir in our scheme).

    Now consider a group of players from $X\cup Y$ that join their forces and at
    least a member of each set is in this group. If the group is of size strictly
    less than $\frac{1}{5}m$, the players have no access to $k,s$. They only have
    access to $x,y$, and therefore $u=x\xor y$ which is of no use to them. If however, this
    group is of size $\frac{1}{5}m$ or more (that is, they form a non-corruptible
    group), they have access to $k$ as well, and can decipher $s=u\xor k$.

    Therefore our scheme fulfills the desired requirements.

\section{Graph non-isomorphism}
    The following solution is largely inspired from \textit{Proofs that yield
    nothing but their validity or all languages in NP have zero-knowledge proof
    systems} by O. Goldreich, S. Micali and A. Wigderson - 1991.

    \textbf{(a)} 
    We propose the following protocol for proving membership in
    $GNI$. The input is a pair of graphs $G_1(\mathcal{V},E_1),G_2(\mathcal{V},E_2)$, where $V$ is a
    set of vertices and $E_1,E_2$ sets of edges.

    \begin{itemize}
        \item 
        The verifier $V$ first choses a random bit $\alpha$, and a permutation
        $\pi$ on $\mathcal{V}$. He constructs $H=\pi(G_\alpha)$.

        He also constructs $n=poly(|\mathcal{V}|)$ pairs of graph such that for each pair, one
        of the graph is isomorphic to $G_0$ and the other to $G_1$. The
        graphs within each of the $n$ pairs are stored in random order. To
        be more precise, for each $i\in\left\{  1\dots n\right\}$, the
        verifier choses a random bit $\gamma_i$, two permutations
        $\tau_{i,0},\tau_{i,1}$ on $\mathcal{V}$ and computes the two graphs
        $T_{i,j}=\tau_{i,j}(G_{j+\gamma_i\ mod\ 2})$, $j\in\left\{ 0,1
        \right\}$. These pairs will be used by the verifier a test against cheating prover.

        The verifier sends $H$ and the sequence $\left[ (T_{1,0},T_{1,1})\dots(T_{n,0},T_{n,1}) \right]$
        to the prover $P$.

        \item 
        The prover choses a binary string $I\in\left\{ 0,1
        \right\}^{n}$ uniformly at random and sends it to the verifier.

        \item
        If $I$ is not a valid string of length $n$, the verifier halts
        and rejects the proof.

        Otherwise, $V$ replies with $\left\{ \gamma_i,\tau_{i,0},\tau_{i,1}
        \right\}_{i:I[i]=1}$ and $\left\{ \alpha + \gamma_i\ mod\
        2,\tau_{i,\alpha + \gamma_i\ mod\ 2}\circ\pi^{-1}\right\}_{i:I[i]=0}$,
        where $I[i]$ is the $i$-th bit of $I$.

        For $i$ such that $I[i]=1$, it means the verifier reveals
        isomorphisms between the inputs graphs and the $i$-th pair previously
        sent (thus proving the graphs are correctly built). For the
        remaining indices, $V$ shows an isomorphism between $H$ and one of
        the input graph (a witness for the proper construction of $H$).

        \item
        The prover checks the mapping sent by $V$ are indeed isomorphisms
        between the adequate graphs. If any of them is incorrect, the prover
        stops. More specifically, $P$ checks that for $i$ s.t. $I[i]=1$,
        $\tau_{i,j}$ is an isomorphic mapping from $T_{i,j}$ and
        $G_{j+\gamma_i\ mod\ 2}$; and for the remaining $i$, $\tau_{i,\alpha
        + \gamma_i\ mod\ 2}\circ\pi^{-1}$ is an isomorphism between $H$ and
        $T_{i,\alpha+\gamma_i\ mod\ 2}$.

        If everything goes well, $P$ replies with $\beta\in\left\{ 0,1
        \right\}$ such that $H$ is isomorphic to $G_\beta$. (If there is no
        such $\beta$, the prover sends $\beta=0$).

        \item
        The verifier checks whether $\alpha=\beta$. If so, he accepts the
        proof. Otherwise, he rejects.
    \end{itemize}

    This protocol is an interactive-proof.
    Completeness is straightforward: if $G_0,G_1$ are not isomorphic, and $P,V$ play
    by the rules, $\alpha=\beta$ and the verifier accepts the proof with
    probability 1.
    For the soundness requirement, if the input graphs are isomorphic, the
    verifier accept with probability $\frac{1}{2}$. Indeed, $H,G_0,G_1$ belong
    to the same equivalence class, therefore $H$ is independent from $\alpha$
    and the probability of having $\alpha=\beta$ is $\frac{1}{2}$.

    \textbf{(b)} Let us show that the aforementioned protocol achieves
    statistical zero-knowledge. For every verifier $V^*$, we construct a
    simulator $M_{V^*}$ that takes as input $(G_0,G_1)$ and whose view is
    statistically close to the view of the interaction. The simulator uses
    $V^*$.

    $M_{V^*}$ proceeds as follows:
    \begin{enumerate}
        \item 
            $M_{V^*}$ initiates $V^*$ with input $(G_0,G_1)$ and a random
            tape $r$ (binary string whose length is polynomially bounded (by the
            running time of $V^*$).
        \item
            $M_{V^*}$ reads the graphs $H$ and the sequence $\left[ (T_{1,0},T_{1,1})\dots(T_{n,0},T_{n,1}) \right]$
            that $V^*$ outputs. 
        \item
            $M_{V^*}$ then choses $I$ like $P$ would do and sent it back to
            $V^*$ (and record it on its log).
        \item
            $M_{V^*}$ reads the output from $V^*$: $\left\{ \gamma_i,\tau_{i,0},\tau_{i,1}
            \right\}_{i:I[i]=1}$ and $\left\{ \alpha + \gamma_i\ mod\
            2,\tau_{i,\alpha + \gamma_i\ mod\ 2}\circ\pi^{-1}\right\}_{i:I[i]=0}$,
            where $I[i]$ is the $i$-th bit of $I$.
        \item
        $M_{V^*}$ checks the mapping sent by $V^*$ are indeed isomorphisms
        between the adequate graphs. If any of them is incorrect, the simulator
        stops. More specifically, $M_{V^*}$ checks that for $i$ s.t. $I[i]=1$,
        $\tau_{i,j}$ is an isomorphic mapping from $T_{i,j}$ and
        $G_{j+\gamma_i\ mod\ 2}$; and for the remaining $i$, $\tau_{i,\alpha
        + \gamma_i\ mod\ 2}\circ\pi^{-1}$ is an isomorphism between $H$ and
        $T_{i,\alpha+\gamma_i\ mod\ 2}$.

        If any of these is malformed, the simulator halts.

        \item
            The simulator $M_{V^*}$ now has to find an isomorphism between $H$
            and $G_\alpha$. The simulator proceeds by 'rewinding' $V^*$. It
            repeats the following steps, until an isomorphism is found. We set an upper
            limit on the number of repetitions that corresponds to enumerating all
            strings: $2^n$. The time complexity of the simulator is discussed
            later on.
            \begin{enumerate}
                \item $M_{V^*}$ chooses a random binary string $K$ of length
                    $n$. It initiates $V^*$ on the same input $(G_0,G_1)$ with
                    the rewound random tape $r$ and sends $K$ to $V^*$'s set
                    request (i.e. where it used to send $I$, step 3 above).

                    Rewind means that $V^*$ will produce the same question and
                    isomorphism pairs
                \item 
                    $M_{V^*}$ reads the output from $V^*$: $\left\{
                    \delta_i,\sigma_{i,0},\sigma_{i,1}
                    \right\}_{i:K[i]=1}$ and $\left\{ \alpha + \delta_i\ mod\
                    2,\sigma_{i,\alpha + \delta_i\ mod\ 2}\circ\pi^{-1}\right\}_{i:K[i]=0}$.
                    ($\delta$ takes the role of $\gamma$ and $\sigma$ is defined
                    similarly to $\tau$ in the previous notation).

                    For $i\in I\cap \bar{K}$, if $\sigma_{i,\alpha + \delta_i\
                    mod\ 2}\circ\pi^{-1}$ is a correct isomorphism between $H$ and
                    $T_{i,\alpha+\delta_i\ mod\ 2}$, then $\tau_{i,\alpha + \delta_i\
                    mod\ 2}^{-1}\circ\sigma_{i,\alpha + \delta_i\ mod\ 2}\circ\pi^{-1}$
                    is an isomorphism between $G_{\alpha}$ and $H$. Similarly,
                    for $i\in \bar{I}\cap K$, we can build and isomorphism between $G_{\alpha}$ and $H$.

                    If an isomorphism cannot be found, we continue looping.
            \end{enumerate}
        \item
        Once an isomorphism between $H$ and $G_\beta$ for some $\beta\in\left\{
        0,1 \right\}$ has been found, $M_{V^*}$ records to its log and returns $\beta$.
    \end{enumerate}

    Let us show that the simulator $M_{V^*}$ runs in expected polynomial time.
    For a fixed random tape $r$, we call \textit{good} a string $I$ such that $V^*$ answers properly on
    request $I$ (the isomorphisms are properly formed). Let $t$ be the number of
    good strings (which depends on $V^*$ and $r$). Of course, $0\leq t \leq n$.
    We compute the expected number of times $V^*$ is invoked. There
    are three cases:

    \begin{itemize}
        \item if $t=0$, $I$ chosen in step 3 is always bad: $M_{V^*}$ halts in
            step 5. $V^*$ is invoked only once (which is polynomial).
        \item if $t=1$, the probability for $I$ chosen in step 3 to be good is
            negligible ($2^{-n}$). If $I$ is bad, $M_{V^*}$ returns, and the
            number of invocations is 1. If $I$ good, step 6 is repeated. But because
            this event happens with negligible probability, $M_{V^*}$ remains
            \textit{expected polynomial time}. In this case, the probability of
            finding a good $K\neq I$ is 0, therefore the simulator will fail.
            This case is also the reason why the scheme is only statistically
            zero-knowledge and not perfect zero-knowledge.
        \item otherwise, if $t\geq 2$, if $I$ chosen in step 3 is bad, the
            number of invocations is exactly one. If $I$ is good, then $M_{V^*}$
            keeps invoking $V^*$ in step 6 till it finds a good $K\neq I$. Thus
            the expected number of invocations is:

            \begin{equation}
                \frac{2^n-t}{2^n}\cdot 1 +
            \frac{t}{2^n}\cdot\left(  \frac{t-1}{2^n}\right)^{-1} \leq 3
            \end{equation}
    \end{itemize}

    In all cases, because we assume $V^*$ is PPT, the simulator runs in expected
    polynomial time.

    Let us complete the proof by showing that $M_{V^*}$'s output is
    statistically indistinguishable from $view(P,V^*)$. This is straightforward
    since both distributions are identical but for the case where $M_{V^*}$
    succeeds in finding a good $I$ in step 3 but fail to find a good $K$ in step
    6. This happens with negligible probability $\frac{t}{2^n}e^{-(t-1)}$

    This completes the proof and our protocol indeed achieves statistical
    zero-knowledge.

    \textbf{(c)}
    This protocol can be repeated sequentially $k$ times, the only requirement
    is the ability to produce \textit{good} strings $K$ in step 6. The protocol
    remains zero-knowledge (nothing changes in the proof above) and the
    simulator still runs in expected polynomial time as long as $k$ is
    polynomial in the size of the graphs. Also, the soundness bound becomes
    $\frac{1}{2^k}$.

    \textbf{(d)}
    $k=O(\log(n)$ parallel repetitions do not harm the zero-knowledge
    characteristic of the protocol. Once again, what matters in the above proof
    is the ability to produce good strings $K$ for each running
    instance\footnote{cf. \textit{Proofs that yield
    nothing but their validity or all languages in NP have zero-knowledge proof
    systems} by O. Goldreich, S. Micali and A. Wigderson - 1991, remark 12}. As
    long as fresh random variables are generated for each repetition, no
    knowledge of the input can leak.

    \textbf{(e)}
    In the previous question, we did not use the fact that $k=O(\log(n))$,
    therefore the protocol remains zero-knowledge for $k=O(n)$. This problem is
    different from the zero-knowledge proof for graph isomorphism which breaks
    for parallel evaluation of $k=O(n)$ instances\footnote{see \textit{Foundations of Cryptography}, section 4.3.2
    p209, Oded Goldreich}.

\section{Graph Hamiltonicity in Zero Knowledge}
    \textbf{(a)}
    If the input graph $G$ is not Hamiltonian and the verifier
    sends $b=0$, $P$ will reply with $\pi$ which $V$ will accept the proof as
    correct. This happens with probability $\frac{1}{2}$, this is the only
    failure case. Therefore, $V$ rejects non Hamiltonian inputs with probability
    a least $\frac{1}{2}$ which makes the protocol \textbf{sound}.

    \textbf{(b)}
    This protocol \textbf{cannot be} \textit{perfect} zero-knowledge. The reason
    for this is that the commitment scheme is only computationally hiding. Suppose
    towards a contradiction that the scheme is zero-knowledge. And let us call
    $S_V$ a perfect simulator (for any verifier $V$) that produces a view
    identical to the one of the interaction between $P$ and $V$. 
    
    Let us call $V^*$ a verifier that always send $b=1$ in step 2 of the
    protocol instead of using any randomness. Since the view of $S_{V^*}$ is to
    match perfectly the one of the actual interaction (perfect zero-knowledge),
    the graphs that the simulator builds have the same distribution as
    permutations of $G$ (the input graph).$V^*$ accepts if and only if the graph
    has a verified hamiltonian cycle ($V^*$ only asks for $b=1$).

    This construction allows us to build an algorithm to determine graph
    hamiltonicity in polynomial, which is not possible, and therefore
    contradicts the fact that the scheme is perfect zero-knowledge.
    The algorithm goes as:

    \begin{itemize}
        \item On input $G$, run the simulator $S_{V^*}(G)$.
        \item if $S_{V^*}$ accepts, return 1, and 0 otherwise.
    \end{itemize}


    \textbf{(c)} With the current scheme, a problem will occur if we try to
    prove computational zero-knowledge. Indeed, $P$ always sends and decommits all
    entries in the adjacency matrix of the graph. When building a simulator $S$ for
    our proof, we will have to specify what $S$ does in the case $b=1$. If the
    output of the simulator is to be computationally indistinguishable from the
    view of the interactive proof, $S$ will have to output the complete
    decommitted adjacency matrix corresponding to $\pi(G)$ as well as a
    Hamiltonian cycle, which is difficult if the simulator does not know a
    Hamiltonian cycle beforehand. The key problem, is that the simulator has to
    reveal the \textbf{whole} graph $\pi(G)$ together with a valid Hamiltonian
    cycle. Revealing only the entries corresponding to the cycle would solve the
    problem since, the output distribution would be identical to the
    distribution of random cycles (as long as $\pi$ is random).

    \textbf{(d)}
    We modify the protocol in step 3. If $b=1$, instead of sending the whole
    decommitted adjacency matrix, $P$ only decommits entries of the matrix that
    represent edges in the Hamiltonian cycle. In step 4, $V$ verifies that the
    decommitments are correct and correspond to $1$s, (i.e. edges in $\pi(G)$).
    $V$ then check that this path is really Hamiltonian. In the case $b=0$ it
    sends all the bits from the adjacency matrix decommitted and $\pi$ as before.

    The soundness bound in this case does not change, and the protocol remains
    sound. Let us now sketch a proof of \textit{computational} zero-knowledge.
    The idea is to build a simulator $S$, that simulates the interaction with
    the prover $P$ and whose view is computationally indistinguishable from the
    view of the interaction between $P$ and $V$. $S$ takes as input a graph $G$
    and proceeds as follow:

    \begin{itemize}
        \item $S$ chooses a random bit $b$.
        \item If $b=0$, it chooses a random permutation $\pi$ on $G$'s vertices
            then uses the commitment scheme to commit the bits in the adjacency
            matrix of $\pi(G)$.
        \item If $b=1$, builds an adjacency matrix like this:
            \begin{itemize}
                \item it chooses a random cycle (that covers all nodes, and has
                    no repeated vertices) and puts a $1$ in the
                    corresponding entries. This makes no assumptions on any
                    graph structure. It just picks a cycle on the set of
                    vertices. This works, because, thanks to our modification,
                    the verifier would not have access to the whole graph in this
                    case.
                \item it puts $0$ in all other entries.
            \end{itemize}
        \item To simulate the interaction with $P$, $S$ outputs $b$ and decommits
            the relevant bits (all if $b=0$, only those corresponding to the
            cycle otherwise).
    \end{itemize}

    In the case $b=1$, and because only the entries corresponding to the
    cycle are decommitted, the output of $S$ is computationally
    indistinguishable from the result of the interaction with $P$ ($P$
    picks a random permutation so the cycle it reveals appears
    random). This works despite the fact that $S$ does not know a
    Hamiltonian cycle in the graph. The version proposed in
    \textbf{(b)} does not have this indistinguishability property,
    because the adjacency matrix is fully decommitted regardless of $b$.
    Therefore the new protocol is both sound and computationally zero-knowledge.

    \textbf{(e)}
    As mentioned in \textbf{(a)}, for a single iteration of the protocol the
    probability that a cheating prover is caught is $\frac{1}{2}$, which is much
    better than the $\frac{1}{|E|}$ probability bound for the 3-coloring ZK
    proof\footnote{see \textit{Foundations of Cryptography}, section 4.4.2.1
    p229, Oded Goldreich}. $E$ is the set of graph edges. The modifications done
    in in \textbf{(d)} do not alter this bound.

    \textbf{(f)} 
    The problem of determining whether $c_0,c_1\in\mathbb{Z}_N^*$ belong
    to the same residuosity class is in $NP$. We know that the graph Hamiltonicity
    problem is $NP$-complete, therefore, via a \textit{Karp} (polynomial time)
    reduction, the zero-knowledge protocol we built can be used to obtain a
    computationally zero-knowledge proof for this new problem (and more generally for all problems in
    $NP$).

    However, it is easy to come up with a perfect zero-knowledge proof for this
    problem, in imitation of the zero-knowledge proof for graph
    isomorphism\footnote{see \textit{Foundations of Cryptography}, section 4.3.8
    p208, Oded Goldreich}. On input $c_0,c_1\in\mathbb{Z}_N^*$, we wish to prove the
    inputs belong to the same residuosity class. We outline the protocol below:

\begin{itemize}
    \item 
        The prover $P$ picks a random $r\in\mathbb{Z}_N^*$ and sends $r^2c_0$ to
        the verifier $V$.
    \item
        $V$ picks $b\in\left\{ 0,1 \right\}$ uniformly at random and sends it
        back to $P$.
    \item
        $P$ replies with a square root $s$ of $r^2c_0c_b^{-1}$ if such a square root
        exists (which must be the case if $c_0,c_1$ are in the same residuosity
        class) or $\perp$ if not.
    \item
        $V$ rejects the proof if it receives $\perp$ or $s$ that is not a square
        root of $r^2c_0c_b^{-1}$. It accepts otherwise.
\end{itemize}

This protocol is correct and sound. It achieves perfect zero-knowledge as can be
seen by adapting the proof for graph isomorphism to the problem at hand.

This scheme is for $c_0,c_1$ that encrypt one bit messages. Repeating the
protocol with ciphers that are the concatenation of one-bit encryption still
yields a zero-knowledge interactive proof (this is similar to the scheme for
graph isomorphism repeated sequentially, which is also zero-knowledge).


\section{One-query functional encryption}
    \textbf{(a)}
    We assume we have at hand a universal circuit $U$ that takes as input a
    function $f$ and some input $x$ and evalutes $f$ on this input, i.e.
    $U(f,x)=f(c)$.
    We propose to complete the scheme as follows:
    \begin{itemize}
        \item $FE.Setup(1^k)\rightarrow (mpk,msk)$
            \begin{itemize}
                \item generate $2m$ key pairs for the public encryption scheme
                    $(pk_{i,b},sk_{i,b})\leftarrow Gen(1^k)$ for $i\in\left\{ 1,\dots, m
                    \right\}$ and $b\in\left\{ 0,1 \right\}$. 
                \item set $msk=(sk_1,\dots,sk_{2m})$ and
                    $mpk=(pk_1,\dots,pk_{2m})$
            \end{itemize}
        \item $FE.KeyGen(msk,f)\rightarrow sk_f$
            \begin{itemize}
                \item we define $f_1\dots f_m\in\left\{ 0,1 \right\}^m$ as the
                    sequence of bits describing $f$ (as a circuit).
                \item output $sk_f=\left( sk_{1,f_1},\dots,sk_{m,f_m} \right)$
            \end{itemize}
        \item $FE.Enc(mpk,x)\rightarrow c$
            \begin{itemize}
                \item Generate a garbled circuit $\hat{U}_x$ of $U(\cdot,x)$
                    using Yao's technique (since we seek to build a system for single query, we can
                    hardcode the input).

                    We define $(k_{i,0},k_{i,1})$ for $i\in\left\{ 1,\dots,m
                    \right\}$, the labels for the $m$ inputs in
                    $\hat{U}_x$ (the labels in each pair correspond to input bits
                    $0,1$ at position $i$).
                \item
                    We define $c_{i,b}=Enc(pk_{i,b},k_i,b)$ for $i\in\left\{
                    1,\dots,m \right\}$, $b\in\left\{ 0,1 \right\}$.

                    Output $c$ that encapsulates the $c_{i,b}$ and $\hat{U}_x$.
            \end{itemize}
        \item $FE.Dec(sk_f,c)\rightarrow f(x)$
            \begin{itemize}
                \item For $i\in\left\{ 1,\dots,m \right\}$ compute
                    $Dec(sk_{i,f_i},c_{i,b})$ for $b=0,1$. Assuming that $Dec$
                    returns $\perp$ if it fails (e.g. trying to decrypt with the
                    wrong key), for each $i$ one of the computation will fail
                    and the other will yield $k_{i,f_i}$, the label for
                    $\hat{U}_x$'s $i$-th input of $\hat{U}_x$ for bit $f_i$.

                    Given the $k_{i,f_i}$, we can feed them into $\hat{U}_x$
                    which is encapsulated in $c$. By definition, this will yield
                    $f(x)$.
            \end{itemize}
    \end{itemize}

    This scheme is correct provided that the public-key encryption scheme and
    the garbling scheme are corrects.

    \textbf{(b)} We assume the garbling scheme is secure. That is, there exists a
    simulator $S_g$ such that for a input circuit $C$ and point $x$, if we
    denote by $\hat{C}$ the garbled circuit and $k_{i,x_i}$ the input labels of the
    garbled circuit, the distributions $(\hat{C},k_{i,x_i})$ and $S_g(C(x),C)$
    are computationally indistinguishable.

    To substitute the call to $FE.Enc$, we build the following simulator $S$:
    \begin{itemize}
        \item Run $S_g(f(x),U(\cdot,x))$ to obtain $k_{i,f_i}$ and $\hat{U}_x$
            that are computationally indistinguishable from the ones build by
            $FE.Enc$. We then encrypt as before, the only difference is that we
            replace the encryption of the $k_{i,1-f_i}$ by random strings to obtain $c$.
    \end{itemize}

    Provided that the garbling is secure and the public encryption scheme
    achieves computational security, the simulator's view is indistinguishable
    from the view of the real algorithm. This proves the required security on
    the Functional Encryption scheme.


\end{document}
