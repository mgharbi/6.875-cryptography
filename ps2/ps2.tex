\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage{amssymb, amsmath, graphicx}
\usepackage[ruled,noline]{algorithm2e}
\usepackage{hyperref}
\usepackage{bbm}

\setlength{\oddsidemargin}{.25in}
\setlength{\evensidemargin}{.25in}
\setlength{\textwidth}{6in}
\setlength{\topmargin}{-0.4in}
\setlength{\textheight}{8.5in}

\newcommand{\heading}[6]{
  \renewcommand{\thepage}{\arabic{page}}
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { \textbf{#2} \hfill #3 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #6  \hfill} }
      \vspace{2mm}
      \hbox to 5.78in { \textit{Collaborators: #4 \hfill #5} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\newenvironment{proof}{\noindent{\bf Proof:} \hspace*{1mm}}{
	\hspace*{\fill} $\Box$ }
\newenvironment{proof_of}[1]{\noindent {\bf Proof of #1:}
	\hspace*{1mm}}{\hspace*{\fill} $\Box$ }
\newenvironment{proof_claim}{\begin{quotation} \noindent}{
	\hspace*{\fill} $\diamond$ \end{quotation}}

\newcommand{\lecturenotes}[5]{\heading{#1}{6.875J/18.425J: Cryptography and Cryptanalysis}{#2}{#5}{#3}{#4}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE MODIFY THESE FIELDS AS APPROPRIATE
\newcommand{\psetnum}{1}          % lecture number
\newcommand{\lecturetitle}{Problem set 1} % topic of lecture
\newcommand{\lecturedate}{\today}  % date of lecture
\newcommand{\studentname}{Michael Gharbi}    % full name of student (i.e., you)
\newcommand{\collaborators}{Andrea Tacchetti, Frank Yi-Fei Wang, Hamza Fawzi}    % full name of student (i.e., you)

\newcommand*\xor{\mathbin{\oplus}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\lecturenotes{\psetnum}{\lecturedate}{\studentname}{\lecturetitle}{\collaborators}
\section{Finding Square Roots and Quadratic Non-Residues}

\textbf{(a)} Since half of the elements in $\mathbb{Z}_p^*$ are quadratic
non-residues ($p$ is prime), we can sample a point $x$ uniformly at random in
the set and test whether or not it is a quadratic residue using the Legendre
symbol $x^{\frac{p-1}{2}}\ mod\ p$ (which can be computed in polynomial time).
We can repeat the operation polynomially many times (unless $x$ is a already a
non-residue). Such a procedures guarantees we find a non-residue in polynomial
($P$) time with overwhelming probability (i.e. greater than $(1-\frac{1}{2^{P(n)}}$)).

\textbf{(b)} We call $\mathcal{B}$ the algorithm of part \textbf{(a)} that
outputs a quadratic non residue. We propose the following algorithm to find
square roots mod $p$:

\begin{algorithm}[!h]
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\Input{$p$ prime, $z$ a quadratic residue}
\Output{$x$ such that $x^2=z$}
\If{$p = 3\ mod\ 4$}{\KwRet $x=z^{\frac{p+1}{4}}\ mod\ p$}
\If{$p = 1\ mod\ 4$}{
    $b \leftarrow \mathcal{B}(p)$\\
    let $l\geq 1,m\ odd$ such that $\frac{p-1}{2}=2^l m$\\
    $\alpha\leftarrow 2^l m$\\
    $\beta \leftarrow 0$\\
    \While{$\alpha=0\ mod\ 2$}{
        $\alpha\leftarrow \frac{\alpha}{2}$\\
        $\beta\leftarrow \frac{\beta}{2}$\\
        \If{$a^\alpha b^\beta = 1\ mod\ p$}{
            $\beta\leftarrow \beta + 2^l m $\\
        }
    }
    \KwRet $x=z^{\frac{\alpha+1}{2}}b^{\frac{\beta}{2}}\ mod\ p$
}
\caption{PPT algorithm to find square roots}\label{algo:roots}
\end{algorithm}

In the case $p= 3\ mod\ 4$, $x=z^{\frac{p+1}{4}}\ mod\ p$ is well defined and
$x^2 = z^{\frac{p+1}{2}}=zz^{\frac{p-1}{2}}=z$ because $z$ is a quadratic
residue. In the other case, $\frac{p-1}{2}=2^l m$ is simply the prime
decomposition of $\frac{p-1}{2}$. The loop terminates because we successively
remove the powers of $2$ from $\alpha$, and the update on $\beta$ is correct
because the power of $2$ in $\beta$ is guaranteed to be higher than the one in
$\alpha$. After an iteration we have $z^{\alpha}b^{\beta}=1\ mod\ p$, so
$z^{\frac{\alpha}{2}}b^{\frac{\beta}{2}}$ is a square root of $1$ and therefore
equals $1$ or $-1$. The if statement deals with the $-1$ case, by multiplying
the term by $-1 = b^{2^l m}$, and brings us back to the standard case ($b$ is a
non residue).

\textbf{(c)} We call $\mathcal{A}$ the algorithm of part \textbf{(b)}, and $QNR$
the set of quadratic non-residues. Recall that membership for this set can be
tested in polynomial time using the Legendre symbol. We propose the following
algorithm to output a unique deterministic quadratic non-residue on input $p$
prime:
\begin{algorithm}[!h]
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\Input{$p$ prime}
\Output{unique quadratic non-residue $r$}
\If{$p = 3\ mod\ 4$}{\KwRet $-1$}
\If{$p = 1\ mod\ 4$}{
    $r \leftarrow -1$\\
    \While{$r\notin QNR$}{
        $ a \leftarrow \mathcal{A}(r)$\\
        \If{$a>\frac{p-1}{2}$}{$a \leftarrow -a$}
        $ r \leftarrow a$
    }
}
\caption{PPT algorithm to output a unique quadratic non-residue}
\label{algo:qnr}
\end{algorithm}
\newpage

Let us prove that the algorithm is correct. If $p = 3\ mod\ 4$, $-1$ is a non-residue:
$p=4k+3$ for some $k$ and $(-1)^{\frac{p-1}{2}}=(-1)^{2k+1}=-1$. In the other
case, $p=4k+1$ for some $k$. Let $g$ be a generator of $\mathbb{Z}_p^*$. There
exist $l\geq 0$ and $m\ odd$ such that $r = -1 = g^{2^l m}$ (prime
decomposition). Initialize $l'=l$, $m'=m$. $r=g^{2^{l'} m'}$.
While $l'>0$ (i.e. $r\notin QNR$), the call to $\mathcal{A}(r)$ outputs a square
root, namely $g^{2^{(l'-1)} m'}$ or $-g^{2^{(l'-1)} m}=g^{2^{(l'-1)} m' + 2^l
m}=g^{2^{(l'-1)} (m' + 2^{l-l'} m)}$. In both cases, the decomposition stays the same
with the power of $2$ decreasing ($m'+2^{l-l'}$ is odd). The choice $a >
\frac{p-1}{2}$ guarantees the output of the algorithm is deterministic by always
picking the same root, even if $\mathcal{A}$ does not. At the end of the loop,
$r=g^m$ with $m$ odd and is therefore a quadratic non-residue.

\section{Properties of generators}
\textbf{(a)} $g$ is a generator of $\mathbb{Z}_N^*$. Let $g'$ be another
generator, then there exists $u$ such that $g = g'^u$. But $g$ being also a
generator, there exists $a$ such that $g' = g^a$. As a consequence we have:

\begin{eqnarray*}
    g &=& g'^{u}\\
    \Leftrightarrow g &=& g^{au}\\
    \Leftrightarrow  1&=& g^{au-1}\\
    \Leftrightarrow  au-1&=&0\ mod\ \phi(N)\\
    \Rightarrow \exists v\ s.t.\ au-v\phi(N)&=&1\\
    \Leftrightarrow gcd(a,\phi(N))&=&1
\end{eqnarray*}

The last equivalence is Bezout's theorem. Conversely, if the last condition
holds, we can find $u,v$ such that $au+\phi(N)v = 1$. Then for all
$x\in\mathbb{Z}_N^*$, there exists $k$ such that we have:

\begin{eqnarray*}
    x &=& g^k\\
    &=& g^{k(au+\phi(N)v)}\\
    &=& (g^a)^{ku}(g^{\phi(N)})^{kv}\\
    &=& (g^a)^{ku}\\
    &=& g'^{ku}
\end{eqnarray*}

Because $g^{\phi(N)} = 1$. This proves that $g'$ is a generator and therefore
the required equivalence.\\

\textbf{(b)} Let $p$ be a prime and $g$ a generator of $\mathbb{Z}_p^*$, and
$\mathcal{A}$ the algorithm described in the problem. Let $g'$ be another
generator. Algorithm \ref{algo:dlog} solves the discrete-log problem using
$\mathcal{A}$ as a subroutine:

\begin{algorithm}[!h]
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\Input{$g'^x$}
\Output{$x'$}
$r\leftarrow$ uniformly at random from $\{1\dots p-1\}$\\
$r'\leftarrow$ uniformly at random from $\{1\dots p-1\}$\\
$\beta \leftarrow \mathcal{A}(g'^r)$\\
$\gamma \leftarrow \mathcal{A}(g'^{xr'})$\\
\KwRet $\frac{\gamma}{r'}/\frac{\beta}{r}$
\caption{PPT algorithm to solve discrete log}
\label{algo:dlog}
\end{algorithm}

With probability $\frac{1}{P(|p|)}$, $\mathcal{A}(g'^r)$ returns $\beta = ar$. And with
the same probability $\mathcal{A}(g'^{xr'})$ returns $\gamma = axr'$. Algorithm
\ref{algo:dlog} therefore returns the correct $x'$ with probability
$\frac{1}{P(|p|)^2}$ ($r$ and $r'$ being sampled independently). In the
algorithm, $a/b$ designates the quotient of the division of $a$ by $b$, the
remainder not being necessarly $0$ in case $\mathcal{A}$ fails.

\section{Jacobi symbol and Primality Testing}
\textbf{(a)} The following algorithm computes the Jacobi symbol $(\frac{a}{N})$:

\begin{algorithm}[!h]
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\Input{$a$,$N$}
\Output{$(\frac{a}{N})$}
$\alpha \leftarrow a$\\
$\nu \leftarrow N$\\
$J\leftarrow 1$\\
\While{}{
    $\alpha \leftarrow \alpha\ mod\ \nu$ /* rule 1 */\\
    \If{$\alpha = 0$}{
        \KwRet $0$
    }
    \While{$\alpha=0\ mod\ 2$}{
        $J \leftarrow$ $(-1)^{\frac{N^2-1}{8}}J$ /* rules 4 and 5* / \\
        $\alpha \leftarrow \frac{\alpha}{2}$\\
    }
    \If{$\alpha = 1$}{
        \KwRet $J$
    }
    $J\leftarrow (-1)^{\frac{(P-1)(Q-1)}{4}}$\\
    $\nu \leftrightarrow \alpha$ /* swap $\nu$,$\alpha$ odd, relatively prime : rule 6* /\\
}
\KwRet 
\caption{PPT algorithm to compute the Jacobi Symbol}
\label{algo:jacobi}
\end{algorithm}
\newpage

The algorithm follows the scheme of successive divisions in Euclid's algorithm
($\alpha \leftarrow \alpha\ mod\ \nu$ and the removal of the factors $2$),
and thus terminates the same way. The rules on the Jacobi symbol computation guarantee that
$J$ hold the correct value at the end of the algorithm. It remains to prove that
$\nu$ and $\alpha$ satisfy the conditions for rule 6 at each iteration. Clearly
both of these are odd: we start with $\nu=N$ odd and remove the power of $2$ in
$\alpha$'s prime decomposition.

\textbf{(b)} We propose the following algorithm to distinguish between primes and
composites:

\begin{algorithm}[!h]
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\Input{$N$}
\Output{``prime'' or ``composite''}
\If{$\exists n$ s.t. $n^2=N$}{\KwRet ``composite''}
\For{ $i=\{ 1\dots k \}$}{
    $x\leftarrow \mathbb{Z}_N^*$ uniformly at random \\
    \If{$(\frac{x}{N})\neq x^{\frac{N-1}{2}}$}{
            \KwRet ``composite''
    }
}
\KwRet ``prime''
\label{algo:jacobi2}
\caption{PPT algorithm to distinguish $N =p$ and $N=pq$}
\end{algorithm}

The search for a square root can be done in polynomial time. We pick $x\in\mathbb{Z}_N^*$ uniformly at random (which can be done in
polynomial time\footnote{cf. \textit{Introduction to Modern Cryptography},
J.Katz,Y.Lindell, Theorem B.16}). We use the algorithm of part \textbf{(a)} to
compute the Jacobi Symbol in polynomial time. The resulting algorithm is then
polynomial. In case $N$ is prime, $(\frac{x}{N})\neq x^{\frac{N-1}{2}}$ never
happens and the algorithm is correct. When $N=pq$ however, we can hope to find
such elements. We consider the case $N$ odd (when $N$ is even, the question of
primality is trivial).

Our algorithm relies on the existence of strong witnesses that $N$ is composite
(for $N$ odd that is not a prime power, i.e. we exclude the case $p=q$) and the
fact that the cardinality of the set of strong witnesses is greater than
$\frac{|\mathbb{Z}_N^*|}{2}$ which is proved in Katz and Lindell\footnote{cf.
\textit{Introduction to Modern Cryptography}, J.Katz,Y.Lindell, Theorem 7.40}.
We also use the fact that if $x$ is not a strong witness, then $a^{N-1}=1\ mod\
N$, in which case we might miss the condition $(\frac{x}{N})\neq
x^{\frac{N-1}{2}}$. For our algorithm to fail, we must pick our $k$ samples only out of the
set of strong witnesses, which, according to the previous remarks, occurs with
probability smaller than $\frac{1}{2^k}$. Our polynomial time algorithm succeeds
with probability greater than $1-\frac{1}{2^k}$ and is PPT.

\section{Bit Security of RSA}

\textbf{(a)} Define $d$ such that $ed = 1\ mod\ \phi(N)$ and $\mathcal{A}$ the algorithm
that extracts the $lsb$ of $m$ on input $m^e$. Let $n=|N|$. We have
$m\in\mathbb{Z}_N^*$ so we can code $m$ on $n$ bits, which the following
algorithm will retrieve:

\begin{algorithm}[!h]
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\Input{$m^e$}
\Output{$B$, the binary representation of $m$}
$l \leftarrow m^e $\\
Precompute $2^{-e}\ mod\ N$\\
\For{$i= 1\dots n$}{
\If{$\mathcal{A}(l) = 0$}{
    $temp[i] = 0$\\
    $ l \leftarrow l*2^{-e}$
}\Else{ 
    /* $\mathcal{A}(l) = 1$ */\\
    $temp[i] = 1$\\
    $ l \leftarrow (N-l)*2^{-e}$
}
}


$B[1] = temp[1]$\\
\For{$i=2\dots n$}{
    \If{$temp[i] = 0$}{
        $B[i] = 0$\\
    }\Else{
        $B[i] = 0$\\
        $B[1\dots i] = comp(B,i,N)$\\
    }
}

\KwRet $B$
\label{algo:rsa}
\end{algorithm}
\newpage

When we do $l \leftarrow (N-l)*2^{-e}$, the message represented by the cipher
$(N-l)=-l\ mod\ N$, $(N-l)^d = (-1)^dl^d = -l^d = N-l^d$ is even because $N$,
$l$ and $d$ are odd ($gcd(d,(p-1)(q-1))=1$ with $(q-1)(p-1)$ even). So, multiplying by
$2^{-e}$ effectively performs a bit-shift to the left operation. (This is a
legal operation since $gcd(2,N)=1$. We are therefore able to move to the next
bit and apply $\mathcal{A}$ to it again, until we
have run through the entire length $n$ of the bit-string. However, when $l$ is
even, the lsb we retrieve (which is 0) is a bit from $l$. But when $l$ is odd,
the lsb (here 1) is a bit from $l$ and all subsequent bits will be bits from
$(N-l)^d$. This is why we have to do some processing to retrieve the bits of $m$
from the sequence of bits $temp$.

In the final answer, $B[1]$ is the most significant bit of $m$ and $B[n]$ the
least significant. While we encounter only zeros in $temp$, it means we are
extracting bits from the same message. But as soon as we encounter a
$1$, it means that we performed a complement to $N$ in the first loop, so the
bits we have seen so far belong to a certain message that is different from the
message whose bits follow the current one, $i$. More precisely, the bits seen so far are the bits of
the complement to $N$ of the truncation of the $i$ most significant bits of the
message whose bits follows (i.e have index strictly greater than $i$ in temp).
So we take the number $\alpha$ represented by these $i$ bits, compute its
complement $N-\alpha$ then extracts the $i$ least significant bits of it and
replace $B[1\dots i]$ with these. When the loop concludes, the message
represented by $B$ is $m$. This algorithm is inspired by the paper
\textit{Why and How to establish a Private Code On a Public Network} by
S.Goldwasser et al. where more details can be found about this post-processing
step.

To illustrate it the algorithm, we wrote a matlab implementation that can be found at
\url{https://bitbucket.org/mgharbi/crypto/src/11b321ff04573723d6dfe694efbe43758c98f25a/ps2/code?at=master}.

\textbf{(b)} If the box is faulty and succeeds with probability greater that
$1-\frac{1}{2n}$, $n=|N|$. The procedure given in \textbf{(a)} will still succeed
with probability greater than $(1-\frac{1}{2n})^n = e^{\ln(1-\frac{1}{2n})}\sim
e^{-\frac{1}{2}+o(\frac{1}{n})}$ which is non negligible and greater than $0.5$
for $n$ sufficiently large.

\section{Square Roots mod Composite $N$}

\textbf{(a)} Let $N=pqr$ with $p,q,r$ odd primes. The Chinese remainder can be
extended with the consequence that $\mathbb{Z}_N^*$ and
$\mathbb{Z}_p^*\times\mathbb{Z}_q^*\times\mathbb{Z}_r^*$ are isomorphs. This
implies that each element in $\mathbb{Z}_N^*$ has exactly $8$ square roots.

Let us prove the existence of such an isomorphism. Let $(x_p,x_q,x_r)\in\mathbb{Z}_p^*\times\mathbb{Z}_q^*\times\mathbb{Z}_r^*$
We have $gcd(qr,p) = 1$ so there exists $m_p$ with $m_pqr = 1\ mod\ p$, let $c_p
= m_pqr$. Define $c_q,c_r$ similarly and let $x=x_p c_p+x_q c_q + x_r c_r$. $x$
is in $\mathbb{Z}_N^*$ because it accepts $x^-1 :=x^{-1}_p c_p+x^{-1}_q c_q +
x^{-1}_r c_r$ as inverse. Furthermore $x$ is unique, because if $y\neq x$
is another possibility, $x$ and $y$ are equals mod $p,q,r$ and therefore
mod $N$ because $p,q,r$ are primes. This proves the existence
of the isomorphism.

Let $z\in\mathbb{Z}_N^*$ and $x$ such that $x^2=z\ mod\ N$, if $x$ is
represented by
$(x_p,x_q,x_r)\in\mathbb{Z}_p^*\times\mathbb{Z}_q^*\times\mathbb{Z}_r^*$. Then,
because the isomorphism preserves the group structure, $z$ is uniquely
represented by $(z_p,z_q,z_r)=(x_p^2,x_q^2,x_r^2)$. This implies that $x_i$ is a
square root of $z_i$, and there only two possibilities for $x_i$ in $Z_i^*$,
where $i=p,q,r$. Therefore there are at best $2^3=8$ possibilities for $x$.
Conversely, all $8$ elements $(\pm x_p^2,\pm x_q^2,\pm x_r^2)$ are solutions in
$\mathbb{Z}_p^*\times\mathbb{Z}_q^*\times\mathbb{Z}_r^*$, which by isomorphism
implies $8$ solutions in $\mathbb{Z}_N^*$.

\textbf{(b)} To distinguish between $N=pqr$ and $N=pq$, we use the follwing
strategy: on input $N$, we generate polynomially many (say $k$) $y_i\in\mathbb{Z}_N^*$ uniformly at random (which
can be done in polynomial time\footnote{cf. \textit{Introduction to Modern
Cryptography}, J.Katz,Y.Lindell, Theorem B.16}) and for each test whether it is a square
using the magic box. The magic box gives us $k$ i.i.d random variables $X_i$ such that
$X_i=1$ if $y_i$ is a square, $X_i=0$ otherwise. We then consider the random
variable $X=\frac{1}{k}\sum_i X_i$ of mean $s$ and variance $\frac{1}{k}s(1-s)$
where $s$ is the proportion of squares in $\mathbb{Z}_N^*$ ($s=s_1 =\frac{1}{8}$
in case $N=pqr$ and $s=s_2 =\frac{1}{4}$ if $N=pq$). The Chebyshev bound
on $X$ gives us :

\begin{equation*}
    \mathbbm{P}[|X-s|\geq\alpha] \leq \frac{s(1-s)}{k\alpha^2}
\end{equation*}

We can set $\alpha = \frac{|s_1-s_2|}{2}$ and let our algorithm output
``$N=pq$'' when $X<s_1 + \alpha$ and ``$N=pqr$'' otherwise. In case $N=pqr$, the
probability that our algorithm is wrong is:

\begin{equation*}
   \mathbbm{P}[X \geq s_1+ \alpha] \leq \mathbbm{P}[|X-s_1|\geq\alpha] \leq
   \frac{s_1(1-s_1)}{k\alpha^2} = \frac{28}{k}
\end{equation*}

For the other case, the probability is less than $\frac{48}{k}$. If we set $k$
accordingly, we have a PPT algorithm to decide the problem at hand.

\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
