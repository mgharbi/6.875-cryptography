function v = bmin(t,u)

    v = zeros(size(u));
    sz = length(t);
    Mt = 0;
    for i = 1:sz
        Mt = Mt + (2^(sz-i)) * t(i);
    end
    sz = length(u);
    Mu = 0;
    for i = 1:sz
        Mu = Mu + (2^(sz-i)) * u(i);
    end

    Mv = Mt - Mu;

    for i = 1:length(u)
        v(i) = mod(Mv,2);
        Mv = floor(Mv / 2);
    end
    v = fliplr(v);

end


